from django.shortcuts import render

from django.urls import reverse_lazy
from django.views import generic

from .models import *
from .forms import *

# Create your views here.

### T E A M ###
  
# list
class ListTeam(generic.View):
    template_name = "core/team/list_team.html"
    context = {}

    def get(self, request, *args, **kwargs):
        #lleva s
        teams = Team.objects.all()
        self.context = {
            "teams": teams
        }
        return render(request, self.template_name, self.context)

# detail
class DetailTeam(generic.View):
    template_name = "core/team/detail_team.html"
    context = {}

    def get(self, request, pk, *args, **kargs):
        #no lleva s
        team = Team.objects.get(pk=pk)
        self.context = {
            "team": team
        }
        return render(request, self.template_name, self.context)

# create
class CreateTeam(generic.CreateView):
    template_name = "core/team/create_team.html"
    context = {}
    form_class = TeamForm
    success_url = reverse_lazy("core:list_team")

# update
class UpdateTeam(generic.UpdateView):
    template_name = "core/team/update_team.html"
    model = Team
    form_class = TeamForm
    success_url = reverse_lazy("core:list_team")

# delete
class DeleteTeam(generic.DeleteView):
    template_name = "core/team/delete_team.html"
    model = Team
    success_url = reverse_lazy("core:list_team")

#-------------------------------------------------------------------------#

### S T A D I U M ###
class ListStadium(generic.View):
    template_name = "core/stadium/list_stadium.html"
    context = {}

    def get(self, request, *args, **kwargs):
        stadiums = Stadium.objects.all()
        self.context = {
            "stadiums": stadiums
        }
        return render(request, self.template_name, self.context)

# detail
class DetailStadium(generic.View):
    template_name = "core/stadium/detail_stadium.html"
    context = {}

    def get(self, request, pk, *args, **kargs):
        #no lleva s
        stadium = Stadium.objects.get(pk=pk)
        self.context = {
            "stadium": stadium
        }
        return render(request, self.template_name, self.context)

# create
class CreateStadium(generic.CreateView):
    template_name = "core/stadium/create_stadium.html"
    context = {}
    form_class = StadiumForm
    success_url = reverse_lazy("core:list_stadium")

# update
class UpdateStadium(generic.UpdateView):
    template_name = "core/stadium/update_stadium.html"
    model = Stadium
    form_class = StadiumForm
    success_url = reverse_lazy("core:list_stadium")

# delete
class DeleteStadium(generic.DeleteView):
    template_name = "core/stadium/delete_stadium.html"
    model = Stadium
    success_url = reverse_lazy("core:list_stadium")

#-------------------------------------------------------------------------#

### C I T Y ###

# list
class ListCity(generic.View):
    template_name = "core/city/list_city.html"
    context = {}

    def get(self, request, *args, **kwargs):
        #lleva s
        citys = City.objects.all()
        self.context = {
            "citys": citys
        }
        return render(request, self.template_name, self.context)

class DetailCity(generic.View):
    template_name = "core/city/detail_city.html"
    context = {}

    def get(self, request, pk, *args, **kargs):
        city = City.objects.get(pk=pk)
        self.context = {
            "city": city
        }
        return render(request, self.template_name, self.context)

# create
class CreateCity(generic.CreateView):
    template_name = "core/city/create_city.html"
    context = {}
    form_class = CityForm
    success_url = reverse_lazy("core:list_city")

# update
class UpdateCity(generic.UpdateView):
    template_name = "core/city/update_city.html"
    model = City
    form_class = CityForm
    success_url = reverse_lazy("core:list_city")

# delete
class DeleteCity(generic.DeleteView):
    template_name = "core/city/delete_city.html"
    model = City
    success_url = reverse_lazy("core:list_city")

### P L A Y E R S ###

# list
class PlayerLIst(generic.View):
    template_name = "core/players/list_player.html"
    context = {}

    def get(self, request, pk, *args, **kwargs):
        players = Players.objects.filter(team__id=pk)
        player = Players.objects.all()
        self.context = {
            "player": player,
            "players": players
        }
        return render(request, self.template_name, self.context)

class PlayerDetail(generic.View):
    template_name = "core/players/detail_player.html"
    context = {}

    def get(self, request, pk, *args, **kargs):
        player = Players.objects.get(pk=pk)
        self.context = {
            "player": player
        }
        return render(request, self.template_name, self.context)

# create
class PlayerCreate(generic.CreateView):
    template_name = "core/players/create_player.html"
    context = {}
    form_class = PlayerForm
    success_url = reverse_lazy("core:list_player")

# update
class PlayerUpdate(generic.UpdateView):
    template_name = "core/players/update_player.html"
    model = Players
    form_class = PlayerForm
    success_url = reverse_lazy("core:list_player")

# delete
class PlayerDelete(generic.DeleteView):
    template_name = "core/players/delete_player.html"
    model = Players
    success_url = reverse_lazy("core:list_player")

### O W N E R S ###

# list
class OwnerList(generic.View):
    template_name = "core/owner/list_owner.html"
    context = {}

    def get(self, request, pk, *args, **kwargs):
        #lleva s
        owner = Owner.objects.all()
        owner2 = Owner.objects.get(id=pk)
        team = Team.objects.get(owner__id=pk)
        stadium = Stadium.objects.get(team__id=team.id)
        self.context = {
            "owner": owner,
            "owner2": owner2,
            "team":team,
            "stadium":stadium

        }
        return render(request, self.template_name, self.context)

class OwnerDetail(generic.View):
    template_name = "core/owner/detail_owner.html"
    context = {}

    def get(self, request, pk, *args, **kargs):
        owner = Owner.objects.get(pk=pk)
        self.context = {
            "owner": owner
        }
        return render(request, self.template_name, self.context)

# create
class OwnerCreate(generic.CreateView):
    template_name = "core/owner/create_owner.html"
    context = {}
    form_class = OwnerForm
    success_url = reverse_lazy("core:list_owner")

# update
class OwnerUpdate(generic.UpdateView):
    template_name = "core/owner/update_owner.html"
    model = Owner
    form_class = OwnerForm
    success_url = reverse_lazy("core:list_owner")

# delete
class OwnerDelte(generic.DeleteView):
    template_name = "core/owner/delete_owner.html"
    model = Owner
    success_url = reverse_lazy("core:list_owner")