from django import forms

from .models import *

# T E A M
class TeamForm(forms.ModelForm):
    class Meta:
        model = Team
        fields = [
            "team_name",
        ]
        widgets = {
            "team_name": forms.TextInput(attrs={"type":"text", "class":"form-control"}),
        }

#-----------------------------------------------------------------------------------------------------------------#

# S T A D I U M
class StadiumForm(forms.ModelForm):
    class Meta:
        model = Stadium
        fields = [
            "stadium_name",
            "team",
        ]
        widgets = {
            "stadium_name": forms.TextInput(attrs={"type":"text", "class":"form-control"}),
            "team": forms.Select(attrs={"type":"select", "class":"form-control"})
        }

# ------------------------------------------------------------------------------------------------------------------#

# C I T Y
class CityForm(forms.ModelForm):
    class Meta:
        model = City
        fields = [
            "city_name",
            "team",
        ]
        widgets = {
            "city_name": forms.TextInput(attrs={"type":"text", "class":"form-control"}),
            "team": forms.Select(attrs={"type":"select", "class":"form-control"})
        }

# P L A Y E R S
class PlayerForm(forms.ModelForm):
    class Meta:
        model = Players
        fields = [
            "name",
            "last_name",
            "team",
        ]
        widgets = {
            "name": forms.TextInput(attrs={"type":"text", "class":"form-control"}),
            "last_name": forms.TextInput(attrs={"type":"text", "class":"form-control"}),
            "team": forms.Select(attrs={"type":"select", "class":"form-control"})
        }

# O W N E R S
class OwnerForm(forms.ModelForm):
    class Meta:
        model = Owner
        fields = [
            "name",
            "last_name",
            "team",
        ]
        widgets = {
            "name": forms.TextInput(attrs={"type":"text", "class":"form-control"}),
            "last_name": forms.TextInput(attrs={"type":"text", "class":"form-control"}),
            "team": forms.Select(attrs={"type":"select", "class":"form-control"})
        }
