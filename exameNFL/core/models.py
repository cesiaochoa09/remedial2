from django.db import models

# Create your models here.

class Team(models.Model):
    team_name = models.CharField(max_length=30, default="Nombre del equipo")
    status = models.BooleanField(default=True)

    def __str__(self):
        return self.team_name


class Stadium(models.Model):
    team = models.ForeignKey(Team, on_delete=models.CASCADE)
    stadium_name = models.CharField(max_length=50, default="Nombre del estadio")
    capacity = models.FloatField()

    def __str__(self):
        return self.stadium_name

class City(models.Model):
    team = models.ForeignKey(Team, on_delete=models.CASCADE)
    city_name = models.CharField(max_length=20, default="Nombre de la ciudad")

    def __str__(self):
        return self.city_name

class Players(models.Model):
    name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    number_player = models.FloatField()
    position = models.CharField(max_length=50)
    team = models.ForeignKey(Team, on_delete=models.CASCADE)

    def __str__(self):
        return self.name
    
class Owner(models.Model):
    name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    team = models.ForeignKey(Team, on_delete=models.CASCADE)

    def __str__(self):
        return self.name
