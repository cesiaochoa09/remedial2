# Generated by Django 5.0 on 2023-12-05 17:33

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0006_remove_city_stadium'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='city',
            name='status',
        ),
        migrations.RemoveField(
            model_name='stadium',
            name='location',
        ),
        migrations.RemoveField(
            model_name='stadium',
            name='status',
        ),
        migrations.RemoveField(
            model_name='team',
            name='director',
        ),
        migrations.RemoveField(
            model_name='team',
            name='players',
        ),
        migrations.CreateModel(
            name='Owner',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('last_name', models.CharField(max_length=50)),
                ('team', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.team')),
            ],
        ),
        migrations.CreateModel(
            name='Players',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('last_name', models.CharField(max_length=50)),
                ('team', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.team')),
            ],
        ),
    ]
