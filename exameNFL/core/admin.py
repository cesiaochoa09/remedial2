from django.contrib import admin

# Register your models here.

from .models import *

@admin.register(Team)
class TeamAdmin(admin.ModelAdmin):
    list_display = [
        "team_name"
    ]

@admin.register(Stadium)
class StadiumAdmin(admin.ModelAdmin):
    list_display = [
        "stadium_name",
        "team",
    ]

@admin.register(City)
class CityAdmin(admin.ModelAdmin):
    list_display = [
        "city_name",
        "team",
    ]

@admin.register(Players)
class Players(admin.ModelAdmin):
    list_display = [
        "name",
        "last_name",
        "team",
    ]

@admin.register(Owner)
class Owner(admin.ModelAdmin):
    list_display = [
        "name",
        "last_name",
        "team",
    ]